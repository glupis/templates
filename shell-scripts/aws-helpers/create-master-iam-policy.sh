#!/bin/bash

#################################################################################################
##                                                                                             ##
##   What to pass as arguments from the command line:                                          ##
##                                                                                             ##
##   -g, --gitlab-runner-account-id, --runner-account-id => 000000000000                       ##
##   -n, --application-name => ApplicationName                                                 ##
##   -e, --environment => PROD/DEV/QA                                                          ##
##   -r, --region => us-east-1                                                                 ##
##   -t, --tags => key=value key=value                                                         ##
##   -p, --profile => aws-profile-name                                                         ##
##                                                                                             ##
##   ** Note that you may have to add different values for different environments **           ##
##                                                                                             ##
#################################################################################################

RESET="\e[0m"
RESET_COLOR="\e[39m"

BOLD="\e[1m"
BLUE="\e[34m${BOLD}"
RED="${RESET}\e[38;5;196m"
YELLOW="\e[33m${BOLD}"
GREEN="\e[32m${BOLD}"

X="${RESET}${BOLD}[${RED}x${RESET_COLOR}${BOLD}]${RESET}"
CHECKMARK="${RESET}${BOLD}[${GREEN}✓${RESET_COLOR}]${RESET}"

for ARG in "$@"; do
  shift
  case "$ARG" in
  '--gitlab-runner-account-id') set -- "$@" '-g' ;;
  '--runner-account-id') set -- "$@" '-g' ;;
  '--application-name') set -- "$@" '-n' ;;
  '--environment') set -- "$@" 'e' ;;
  '--region') set -- "$@" '-r' ;;
  '--tags') set -- "$@" '-t' ;;
  '--profile') set -- "$@" '-p' ;;
  *) set -- "$@" "$ARG" ;;
  esac
done

while getopts g:n:e:r:t:p: ARG; do
  case $ARG in
  g) RUNNER_ACCOUNT_ID=$OPTARG ;;
  n) APPLICATION_NAME=$OPTARG ;;
  e) ENVIRONMENT=$OPTARG ;;
  r) REGION=$OPTARG ;;
  t) read -r -a TAGSET <<<"$OPTARG" ;;
  p) PROFILE=$OPTARG ;;
  *) usage ;;
  esac
done

RUNNER_ROOT_ARN="arn:aws:iam::${RUNNER_ACCOUNT_ID}:root"
echo "${APPLICATION_NAME}"
echo "${ENVIRONMENT}"
echo "${REGION}"
#echo "${MAPFILE[@]}"
echo "${TAGSET[@]}"

TAGS=""
for TAG in "${TAGSET[@]}"; do
  echo "${TAG}"
  if [[ -n $TAG ]] && [[ $TAG != " " ]]; then
    if [[ $TAGS == "" ]]; then
      TAGS="{\"Key\":\"$(echo "$TAG" | cut -d "=" -f1)\",\"Value\":\"$(echo "$TAG" | cut -d "=" -f2)\"}"
    else
      TAGS="$TAGS,{\"Key\":\"$(echo "$TAG" | cut -d "=" -f1)\",\"Value\":\"$(echo "$TAG" | cut -d "=" -f2)\"}"
    fi
  fi
done

TAGS="[$TAGS]"
echo "$TAGS"

ROLE_NAME="${APPLICATION_NAME}GitLabRunnerAssumeRole${ENVIRONMENT}"
#RUNNER_ROOT_ARN="arn:aws:iam::${RUNNER_ACCOUNT_ID}:root"
ROLE_NAME="GitLabRunnerAssumeRoleForIAM"

TRUSTED_POLICY="\
{\
  \"Version\": \"2012-10-17\",\
  \"Statement\": [\
    {\
      \"Sid\": \"AllowAssumeRole\",\
      \"Effect\": \"Allow\",\
      \"Principal\": {\
        \"AWS\": \"${RUNNER_ROOT_ARN}\"\
      },\
      \"Action\": \"sts:AssumeRole\"\
    }\
  ]\
}"


echo -e "${BLUE}Creating/Finding the IAM Role ${ROLE_NAME}...${RED}"
ROLE_ARN=$(
  (
    (
      aws iam get-role --role-name "${ROLE_NAME}" --profile "${PROFILE}" ||
        aws iam create-role \
          --role-name "${ROLE_NAME}" \
          --assume-role-policy-document "${TRUSTED_POLICY}" \
          --tags "${TAGS}" \
          --profile "${PROFILE}"
    ) | jq ".Role.Arn" | tr -d "\""
  ) || echo "UNDEFINED"
)

if [[ -z "${ROLE_ARN}" ]]; then
  echo -e "${X} The IAM Role ${BOLD}${ROLE_NAME}${RESET} was unable to be created/found"
else
  echo -e "${CHECKMARK} Created/Found the IAM Role ${BOLD}${ROLE_NAME}"
fi

echo -e "${BLUE}Updating the IAM Role ${ROLE_NAME}...${RED}"
(
  aws iam update-assume-role-policy --role-name "${ROLE_NAME}" --policy-document "${TRUSTED_POLICY}" --profile "${PROFILE}" &&
    echo -e "${CHECKMARK} Successfully updated the IAM Role ${BOLD}${ROLE_NAME}"
) || echo -e "${X} The IAM Role ${BOLD}${ROLE_NAME}${RESET} was unable to be updated"

CreatePolicy() {
  POLICY_TYPE=$(echo "$1" | cut -d " " -f1)
  POLICY=$(echo "$1" | cut -d " " -f2)
  (
    (
      #        --policy-name "${POLICY_TYPE}Policy" \
      aws iam put-role-policy \
        --role-name "${ROLE_NAME}" \
        --policy-name "${APPLICATION_NAME}${POLICY_TYPE}Policy${ENVIRONMENT}" \
        --policy-document "${POLICY}" \
        --profile "${PROFILE}"
    ) && echo -e "${CHECKMARK} Successfully added/updated the ${BOLD}${POLICY_TYPE}${RESET} Policy to the IAM Role ${ROLE_NAME}${RED}"
  ) || echo -e "${X} Unable to add/update the ${BOLD}${POLICY_TYPE}${RESET} Policy to the IAM Role ${ROLE_NAME}${RED}"
}

IAM_MASTER_POLICY=$(echo "\
{\
  \"Version\": \"2012-10-17\",\
  \"Statement\": [\
    {\
      \"Sid\": \"AllowBasicIAM\",\
      \"Effect\": \"Allow\",\
      \"Action\": [\
        \"iam:PassRole\",\
        \"iam:GetPolicyVersion\",\
        \"iam:AttachRolePolicy\",\
        \"iam:DetachRolePolicy\",\
        \"iam:TagRole\",\
        \"iam:UntagRole\",\
        \"iam:ListRoles\",\
        \"iam:ListRoleTags\",\
        \"iam:TagPolicy\",\
        \"iam:UntagPolicy\",\
        \"iam:CreateRole\",\
        \"iam:GetRole\",\
        \"iam:UpdateRole\",\
        \"iam:DeleteRole\",\
        \"iam:GetRolePolicy\",\
        \"iam:PutRolePolicy\",\
        \"iam:UpdateAssumeRolePolicy\",\
        \"iam:DeleteRolePolicy\",\
        \"iam:ListRolePolicies\",\
        \"iam:ListAttachedRolePolicies\",\
        \"iam:GetPolicy\",\
        \"iam:UpdatePolicy\",\
        \"iam:DeletePolicy\",\
        \"iam:PutPolicy\",\
        \"iam:ListPolicies\",\
        \"iam:ListPolicyTags\",\
        \"iam:ListEntitiesForPolicy\",\
        \"iam:CreatePolicyVersion\",\
        \"iam:DeletePolicyVersion\",\
        \"iam:ListPolicyVersions\",\
        \"iam:SetDefaultPolicyVersion\"\
      ],\
      \"Resource\": [\
        \"*\"\
      ]\
    }\
  ]\
}" | tr -d " ")

CreatePolicy "IAMRoleCreation ${IAM_MASTER_POLICY}"