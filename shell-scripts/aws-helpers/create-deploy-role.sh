#!/bin/bash

# Bash Styling
RESET="\e[0m"
RESET_COLOR="\e[39m"
BOLD="\e[1m"
BLUE="\e[34m${BOLD}"
RED="${RESET}\e[38;5;196m"
YELLOW="\e[33m${BOLD}"
GREEN="\e[32m${BOLD}"
X="${RESET}${BOLD}[${RED}x${RESET_COLOR}${BOLD}]${RESET}"
CHECKMARK="${RESET}${BOLD}[${GREEN}✓${RESET_COLOR}]${RESET}"

# Transform Long Arguments to Short Arguments
for ARG in "$@"; do
  shift
  case "$ARG" in
    '--aws-account-id') set -- "$@" '-i' ;;
    '--gitlab-runner-account-id') set -- "$@" '-g' ;;
    '--runner-account-id') set -- "$@" '-g' ;;
    '--application-name') set -- "$@" '-n' ;;
    '--environment') set -- "$@" 'e' ;;
    '--region') set -- "$@" '-r' ;;
    '--tags') set -- "$@" '-t' ;;
    '--profile') set -- "$@" '-p' ;;
    *) set -- "$@" "$ARG" ;;
  esac
done

# Short Arguments
while getopts i:g:n:e:r:t:p: ARG; do
  case $ARG in
    i) ACCOUNT_ID=$OPTARG ;;
    g) RUNNER_ACCOUNT_ID=$OPTARG ;;
    n) APPLICATION_NAME=$OPTARG ;;
    e) ENVIRONMENT=$OPTARG ;;
    r) REGION=$OPTARG ;;
    t) read -r -a TAGSET <<< "$OPTARG" ;;
    p) PROFILE=$OPTARG ;;
    *) usage ;;
  esac
done

# For Local Testing
cat build.env && echo "" > build.env

# Parsing Tags to a Usable Form for the AWS CLI
TAGS=""
KEYS=()

declare -A TAGS_FOR_DEPLOY

for TAG in "${TAGSET[@]}"; do
  if [[ -n $TAG ]] && [[ $TAG != " " ]]; then
    KEY="$(echo "$TAG" | cut -d "=" -f1)"
    VALUE="$(echo "$TAG" | cut -d "=" -f2)"
    if [[ $TAGS == "" ]]; then
      TAGS="{\"Key\":\"${KEY}\",\"Value\":\"${VALUE}\"}"
    else
      TAGS="$TAGS,{\"Key\":\"${KEY}\",\"Value\":\"${VALUE}\"}"
    fi
    KEYS+=("$KEY")
    TAGS_FOR_DEPLOY[$KEY]=$VALUE
  fi
done

TAGS="[$TAGS]"

for KEY in "${KEYS[@]}"; do
  echo "$(echo "$KEY" | tr '[:lower:]' '[:upper:]' | tr " " "_")=${TAGS_FOR_DEPLOY[$KEY]}" >> build.env
done

# Configuring Variables Needed for AWS
RUNNER_ROOT_ARN="arn:aws:iam::${RUNNER_ACCOUNT_ID}:root"
ROLE_NAME="${APPLICATION_NAME}GitLabRunnerAssumeRole${ENVIRONMENT}"
LOWERCASE_APPLICATION_NAME="$(echo "${APPLICATION_NAME}" | sed -e 's|\([A-Z][^A-Z]\)| \1|g' -e 's|\([a-z]\)\([A-Z]\)|\1 \2|g' | sed 's/^ *//g' | tr '[:upper:]' '[:lower:]' | tr " " "-")-$(echo "${ENVIRONMENT}" | tr '[:upper:]' '[:lower:]')"
SAM_MANAGED_BUCKET="$(echo "${APPLICATION_NAME}" | sed -e 's|\([A-Z][^A-Z]\)| \1|g' -e 's|\([a-z]\)\([A-Z]\)|\1 \2|g' | sed 's/^ *//g' | tr '[:upper:]' '[:lower:]' | tr " " "-")-sam-managed-$(echo "${ENVIRONMENT}" | tr '[:upper:]' '[:lower:]')"

# Creating an S3 Bucket Name and Tags as Environment Variables for Jobs Later in the Pipeline
echo "LOWERCASE_APPLICATION_NAME=${LOWERCASE_APPLICATION_NAME}" >> build.env
echo "SAM_MANAGED_BUCKET=${SAM_MANAGED_BUCKET}" >> build.env

# Policy that Allows the GitLab Runner to Assume this Account
TRUSTED_POLICY="\
{\
  \"Version\": \"2012-10-17\",\
  \"Statement\": [\
    {\
      \"Sid\": \"AllowAssumeRole\",\
      \"Effect\": \"Allow\",\
      \"Principal\": {\
        \"AWS\": [\
          \"${RUNNER_ROOT_ARN}\"
        ]\
      },\
      \"Action\": \"sts:AssumeRole\"\
    }\
  ]\
}"

# GitLab Runner IAM Role Creation
echo -e "${BLUE}Creating/Finding the IAM Role ${ROLE_NAME}...${RED}"
(
  ( (aws iam get-role --role-name "${ROLE_NAME}" --region "$REGION" --profile "$PROFILE" || aws iam get-role --role-name "${ROLE_NAME}" --region "$REGION") > /dev/null) ||
    (
      echo -e "${X} The IAM Role ${BOLD}${RED}${ROLE_NAME}${RESET} was not found"
      echo -e "${BLUE}Attempting to create the IAM Role ${ROLE_NAME}...${RED}"

      (
        (
          aws iam create-role \
            --role-name "${ROLE_NAME}" \
            --assume-role-policy-document "${TRUSTED_POLICY}" \
            --tags "${TAGS}" \
            --profile "$PROFILE" ||
            aws iam create-role \
              --role-name "${ROLE_NAME}" \
              --assume-role-policy-document "${TRUSTED_POLICY}" \
              --tags "${TAGS}"
        ) > /dev/null

      ) ||
        (
          echo -e "${X} The IAM Role ${BOLD}${ROLE_NAME}${RESET} was unable to be created" && exit 1
        )
      echo -e "${CHECKMARK} Created the IAM Role ${BOLD}${GREEN}${ROLE_NAME}"
    )
) && echo -e "${CHECKMARK} Created/Found the IAM Role ${BOLD}${GREEN}${ROLE_NAME}"

# Updates the GitLab Runner IAM Role to keep all Policies up to date
echo -e "${BLUE}Updating the IAM Role ${ROLE_NAME}...${RED}"
(
  (
    aws iam update-assume-role-policy --role-name "${ROLE_NAME}" --policy-document "${TRUSTED_POLICY}" --region "$REGION" --profile "$PROFILE" ||
      aws iam update-assume-role-policy --role-name "${ROLE_NAME}" --policy-document "${TRUSTED_POLICY}" --region "$REGION"
  ) &&
    echo -e "${CHECKMARK} Successfully updated the IAM Role ${BOLD}${GREEN}${ROLE_NAME}"
) || (echo -e "${X} The IAM Role ${BOLD}${ROLE_NAME}${RESET} was unable to be updated" && exit 1)

# Least access needed for Cloud Formation
CLOUD_FORMATION_POLICY=$(echo "\
{\
  \"Version\": \"2012-10-17\",\
  \"Statement\": [\
    {\
      \"Sid\": \"AllowBasicCloudFormation\",\
      \"Effect\": \"Allow\",\
      \"Action\": [\
        \"cloudformation:CreateChangeSet\",\
        \"cloudformation:DescribeChangeSet\",\
        \"cloudformation:DeleteChangeSet\",\
        \"cloudformation:ExecuteChangeSet\",\
        \"cloudformation:DescribeStacks\",\
        \"cloudformation:DescribeStackEvents\",\
        \"cloudformation:GetTemplateSummary\"\
      ],\
      \"Resource\": [\
        \"arn:aws:s3:::${SAM_MANAGED_BUCKET}/*\",\
        \"arn:aws:cloudformation:${REGION}:${ACCOUNT_ID}:stack/${APPLICATION_NAME}${ENVIRONMENT}/*\",\
        \"arn:aws:cloudformation:${REGION}:aws:transform/Serverless-2016-10-31\"\
      ]\
    },\
    {\
      \"Sid\": \"AllowBasicCloudFormationResources\",\
      \"Effect\": \"Allow\",\
      \"Action\": [\
        \"cloudformation:DeleteResource\",\
        \"cloudformation:GetResource\",\
        \"cloudformation:UpdateResource\",\
        \"cloudformation:CreateResource\"\
      ],\
      \"Resource\": [\
        \"*\"\
      ]\
    }\
  ]\
}" | tr -d " ")

# LEAST ACCESS NEEDED FOR LOGS
CLOUDWATCH_POLICY=$(echo "\
{\
  \"Version\": \"2012-10-17\",\
  \"Statement\": [\
    {\
      \"Sid\": \"AllowBasicLogs\",\
      \"Effect\": \"Allow\",\
      \"Action\": [\
        \"logs:CreateLogGroup\",\
        \"logs:DescribeLogGroups\",\
        \"logs:DeleteLogGroup\",\
        \"logs:TagResource\",\
        \"logs:UntagResource\",\
        \"logs:ListTagsForResource\",\
        \"logs:UntagLogGroup\",\
        \"logs:TagLogGroup\",\
        \"logs:ListTagsLogGroup\"\
      ],\
      \"Resource\": [\
        \"arn:aws:logs:${REGION}:${ACCOUNT_ID}:log-group:/aws/events/${APPLICATION_NAME}*${ENVIRONMENT}:log-stream:\",\
        \"arn:aws:logs:${REGION}:${ACCOUNT_ID}:log-group:/aws/events/${APPLICATION_NAME}*${ENVIRONMENT}\",\
        \"arn:aws:logs:${REGION}:${ACCOUNT_ID}:log-group:${APPLICATION_NAME}*${ENVIRONMENT}:log-stream:\",\
        \"arn:aws:logs:${REGION}:${ACCOUNT_ID}:log-group:${APPLICATION_NAME}*${ENVIRONMENT}\",\
        \"arn:aws:logs:${REGION}:${ACCOUNT_ID}:log-group:*${LOWERCASE_APPLICATION_NAME}:log-stream:\",\
        \"arn:aws:logs:${REGION}:${ACCOUNT_ID}:log-group:*${LOWERCASE_APPLICATION_NAME}\",\
        \"arn:aws:logs:${REGION}:${ACCOUNT_ID}:log-group::log-stream:\"\
      ]\
    },\
    {\
      \"Sid\": \"AllowBasicLogsResources\",\
      \"Effect\": \"Allow\",\
      \"Action\": [\
        \"logs:CreateLogDelivery\",\
        \"logs:GetLogDelivery\",\
        \"logs:ListLogDeliveries\",\
        \"logs:UpdateLogDelivery\",\
        \"logs:PutResourcePolicy\",\
        \"logs:DescribeResourcePolicies\",\
        \"logs:DeleteResourcePolicy\"\
      ],\
      \"Resource\": [\
        \"*\"\
      ]\
    }\
  ]\
}" | tr -d " ")

# LEAST ACCESS NEEDED FOR S3
S3_POLICY=$(echo "\
{\
  \"Version\": \"2012-10-17\",\
  \"Statement\": [\
    {\
      \"Sid\": \"AllowBasicS3\",\
      \"Effect\": \"Allow\",\
      \"Action\": [\
        \"s3:PutObject\",\
        \"s3:GetObject\",\
        \"s3:ListBucket\",\
        \"s3:ListObjects\",\
        \"s3:DeleteObject\",\
        \"s3:GetBucketTagging\",\
        \"s3:GetBucketVersioning\",\
        \"s3:PutBucketVersioning\",\
        \"s3:PutBucketPublicAccessBlock\",\
        \"s3:GetBucketPublicAccessBlock\",\
        \"s3:PutPublicAccessBlock\",\
        \"s3:GetPublicAccessBlock\",\
        \"s3:GetAccountPublicAccessBlock\",\
        \"s3:PutBucketOwnershipControls\",\
        \"s3:GetBucketOwnershipControls\"\
      ],\
      \"Resource\": [\
        \"arn:aws:s3:::${SAM_MANAGED_BUCKET}/*\",\
        \"arn:aws:s3:::${SAM_MANAGED_BUCKET}\",\
        \"arn:aws:s3:::*${LOWERCASE_APPLICATION_NAME}/*\",\
        \"arn:aws:s3:::*${LOWERCASE_APPLICATION_NAME}\"\
      ]\
    },\
    {\
      \"Sid\": \"AllowBasicS3Resources\",\
      \"Effect\": \"Allow\",\
      \"Action\": [\
        \"s3:CreateBucket\",\
        \"s3:DeleteBucket\",\
        \"s3:PutBucketTagging\",\
        \"s3:GetBucketPolicy\",\
        \"s3:PutBucketPolicy\",\
        \"s3:DeleteBucketPolicy\",\
        \"s3:PutBucketPublicAccessBlock\",\
        \"s3:GetAccountPublicAccessBlock\"\
      ],\
      \"Resource\": [\
        \"*\"\
      ]\
    }\
  ]\
}" | tr -d " ")

# LEAST ACCESS NEEDED FOR LAMBDA
LAMBDA_POLICY=$(echo "\
{\
  \"Version\": \"2012-10-17\",\
  \"Statement\": [\
    {\
      \"Sid\": \"AllowBasicLambda\",\
      \"Effect\": \"Allow\",\
      \"Action\": [\
        \"lambda:CreateFunction\",\
        \"lambda:GetFunction\",\
        \"lambda:UpdateFunctionConfiguration\",\
        \"lambda:UpdateFunctionCode\",\
        \"lambda:DeleteFunction\",\
        \"lambda:ListTags\",\
        \"lambda:TagResource\",\
        \"lambda:UntagResource\",\
        \"lambda:AddPermission\",\
        \"lambda:RemovePermission\",\
        \"lambda:GetLayerVersion\",\
        \"lambda:ListLayerVersions\",\
        \"lambda:PublishLayerVersion\",\
        \"lambda:DeleteLayerVersion\",\
        \"lambda:AddLayerVersionPermission\",\
        \"lambda:RemoveLayerVersionPermission\",\
        \"lambda:PutFunctionEventInvokeConfig\",\
        \"lambda:UpdateFunctionEventInvokeConfig\",\
        \"lambda:DeleteFunctionEventInvokeConfig\"\
      ],\
      \"Resource\": [\
        \"arn:aws:lambda:*:${ACCOUNT_ID}:layer:${APPLICATION_NAME}*:*\",\
        \"arn:aws:lambda:*:${ACCOUNT_ID}:layer:${APPLICATION_NAME}*\",\
        \"arn:aws:lambda:*:${ACCOUNT_ID}:function:${APPLICATION_NAME}*${ENVIRONMENT}\",\
        \"arn:aws:lambda:*:${ACCOUNT_ID}:function:${APPLICATION_NAME}*${ENVIRONMENT}:*\"\
      ]\
    }\
  ]\
}" | tr -d " ")

# LEAST ACCESS NEEDED FOR IAM
IAM_POLICY=$(echo "\
{\
  \"Version\": \"2012-10-17\",\
  \"Statement\": [\
    {\
      \"Sid\": \"AllowBasicIAM\",\
      \"Effect\": \"Allow\",\
      \"Action\": [\
        \"iam:PassRole\",\
        \"iam:AttachRolePolicy\",\
        \"iam:DetachRolePolicy\",\
        \"iam:TagRole\",\
        \"iam:UntagRole\",\
        \"iam:TagPolicy\",\
        \"iam:UntagPolicy\",\
        \"iam:CreateRole\",\
        \"iam:GetRole\",\
        \"iam:UpdateRole\",\
        \"iam:UpdateAssumeRolePolicy\",\
        \"iam:DeleteRole\",\
        \"iam:GetRolePolicy\",\
        \"iam:PutRolePolicy\",\
        \"iam:DeleteRolePolicy\"\
      ],\
      \"Resource\": [\
        \"arn:aws:iam::${ACCOUNT_ID}:role/${APPLICATION_NAME}*\",\
        \"arn:aws:iam::${ACCOUNT_ID}:policy/${APPLICATION_NAME}*\"\
      ]\
    }\
  ]\
}" | tr -d " ")

# LEAST ACCESS NEEDED FOR EventBridgeError
EVENTBRIDGE_POLICY=$(echo "\
{\
  \"Version\": \"2012-10-17\",\
  \"Statement\": [\
    {\
      \"Sid\": \"AllowBasicEventBridge\",\
      \"Effect\": \"Allow\",\
      \"Action\": [\
        \"events:TagResource\",\
        \"events:UntagResource\",\
        \"events:CreateEventBus\",\
        \"events:DeleteEventBus\",\
        \"events:PutPermission\",\
        \"events:RemovePermission\",\
        \"events:PutEvents\",\
        \"events:DescribeEventBus\",\
        \"events:DescribeRule\",\
        \"events:PutTargets\",\
        \"events:RemoveTargets\",\
        \"events:PutRule\",\
        \"events:DeleteRule\",\
        \"events:CreateArchive\",\
        \"events:DescribeArchive\",\
        \"events:UpdateArchive\",\
        \"events:DeleteArchive\"\
      ],\
      \"Resource\": [\
        \"arn:aws:events:${REGION}:${ACCOUNT_ID}:rule/${APPLICATION_NAME}*\",\
        \"arn:aws:events:${REGION}:${ACCOUNT_ID}:archive/${APPLICATION_NAME}Archive${ENVIRONMENT}\",\
        \"arn:aws:events:${REGION}:${ACCOUNT_ID}:event-bus/default\"\
      ]\
    },\
    {\
      \"Sid\": \"AllowBasicEventBridgeResources\",\
      \"Effect\": \"Allow\",\
      \"Action\": [\
        \"events:ListArchives\"\
      ],\
      \"Resource\": [\
        \"*\"\
      ]\
    }\
  ]\
}" | jq -c '.')

SQS_POLICY=$(echo "\
{\
  \"Version\": \"2012-10-17\",\
  \"Statement\": [\
    {\
      \"Sid\": \"AllowBasicSQS\",\
      \"Effect\": \"Allow\",\
      \"Action\": [\
        \"sqs:createqueue\",\
        \"sqs:getqueueattributes\",\
        \"sqs:setqueueattributes\",\
        \"sqs:listqueuetags\",\
        \"sqs:tagqueue\",\
        \"sqs:untagqueue\",\
        \"sqs:deletequeue\"\
      ],\
      \"Resource\": [\
        \"arn:aws:sqs:${REGION}:${ACCOUNT_ID}:${APPLICATION_NAME}DeadLetterQueue${ENVIRONMENT}\"\
      ]\
    }\
  ]\
}" | jq -c '.')

SSM_POLICY=$(echo "\
{\
  \"Version\": \"2012-10-17\",\
  \"Statement\": [\
    {\
      \"Sid\": \"AllowBasicSSM\",\
      \"Effect\": \"Allow\",\
      \"Action\": [\
        \"ssm:SendCommand\"\
      ],\
      \"Resource\": [\
        \"arn:aws:ec2:${REGION}:${ACCOUNT_ID}:instance/*\",\
        \"arn:aws:ssm:${REGION}::document/AWS-RunShellScript\"\
      ]\
    },\
    {\
      \"Sid\": \"AllowSSMGetCommandInvocation\",\
      \"Effect\": \"Allow\",\
      \"Action\": [\
        \"ssm:GetCommandInvocation\"\
      ],\
      \"Resource\": [\
        \"arn:aws:ssm:${REGION}:${ACCOUNT_ID}:*\"\
      ]\
    }\
  ]\
}" | jq -c '.')

# Adding All Policies to an Array to Make Creation Simpler
POLICIES=("S3 ${S3_POLICY}" "CloudFormation ${CLOUD_FORMATION_POLICY}" "Lambda ${LAMBDA_POLICY}" "IAM ${IAM_POLICY}" "EventBridge ${EVENTBRIDGE_POLICY}" "CloudWatch ${CLOUDWATCH_POLICY}" "SQS ${SQS_POLICY}" "SSM ${SSM_POLICY}")

# Helper Variables for Printing to the Terminal
SHOULD_FAIL=0
FAILED=0

# Creates the Policies for the GitLab Runner Role as Inline
CreatePolicy() {
  # Helper Variables for Naming
  POLICY_TYPE="$1"
  POLICY="$2"

  (
    (
      aws iam put-role-policy \
        --role-name "${ROLE_NAME}" \
        --policy-name "${APPLICATION_NAME}${POLICY_TYPE}Policy${ENVIRONMENT}" \
        --policy-document "${POLICY}" \
        --profile "$PROFILE" ||
        aws iam put-role-policy \
          --role-name "${ROLE_NAME}" \
          --policy-name "${APPLICATION_NAME}${POLICY_TYPE}Policy${ENVIRONMENT}" \
          --policy-document "${POLICY}"
    ) && # Success
      (
        echo "0"
      )
  ) || # Failure
    (
      echo "1"
    )
}

# Updates the Inline Policies for the GitLab Runner Role
echo -e "${BLUE}Adding/Updating inline policies for the IAM Role ${ROLE_NAME}...${RED}"
for POLICY in "${POLICIES[@]}"; do
  # Helper Variables for Naming
  POLICY_TYPE=$(echo "$POLICY" | cut -d " " -f1)
  POLICY=$(echo "$POLICY" | cut -d " " -f2)
  FAILED=$(CreatePolicy "${POLICY_TYPE}" "${POLICY}")

  if [ "$FAILED" == 1 ]; then # Job Will Fail With Warnings
    echo -e "${X} Unable to add/update the ${BOLD}${GREEN}${POLICY_TYPE}${RESET} Policy to the IAM Role ${BOLD}${GREEN}${ROLE_NAME}${RED}"
    SHOULD_FAIL=1
    FAILED=0
  else
    echo -e "${CHECKMARK} Successfully added/updated the ${BOLD}${GREEN}${POLICY_TYPE}${RESET} Policy to the IAM Role ${BOLD}${GREEN}${ROLE_NAME}${RED}"
  fi
done

# TODO: Fix this because it never fails with errors
exit $SHOULD_FAIL