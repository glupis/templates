#!/bin/bash

# Bash Styling
RESET="\e[0m"
RESET_COLOR="\e[39m"
BOLD="\e[1m"
BLUE="\e[34m${BOLD}"
RED="${RESET}\e[38;5;196m"
YELLOW="\e[33m${BOLD}"
GREEN="\e[32m${BOLD}"
X="${RESET}${BOLD}[${RED}x${RESET_COLOR}${BOLD}]${RESET}"
CHECKMARK="${RESET}${BOLD}[${GREEN}✓${RESET_COLOR}]${RESET}"

# Transform Long Arguments to Short Arguments
for ARG in "$@"; do
  shift
  case "$ARG" in
    '--bucket-name') set -- "$@" '-b' ;;
    '--files') set -- "$@" '-f' ;;
    *) set -- "$@" "$ARG" ;;
  esac
done

# Short Arguments
while getopts b:f: ARG; do
  case $ARG in
    b) BUCKET_NAME=$OPTARG ;;
    f) FILES=$OPTARG ;;
    *) usage ;;
  esac
done

# Deploying the Build to the S3 Bucket
echo -e "${BLUE}Deploying and synchronizing the application build files to the S3 Bucket $BUCKET_NAME...${YELLOW}"
(
  aws s3 sync "$FILES" "s3://$BUCKET_NAME" --delete &&
    echo -e "${CHECKMARK} Successfully deployed and synchronized the application build files to the S3 Bucket $BUCKET_NAME"
) || (echo -e "${X} Failure deploying and synchronizing the application build files to the S3 Bucket $BUCKET_NAME" && exit 1)

if [ -z "$(alias sam)" ] && ! where sam | grep -qi '.cmd'; then
  echo 'found .cmd'
  alias sam='sam.cmd'
fi