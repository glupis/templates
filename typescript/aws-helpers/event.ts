import * as AWS from 'aws-sdk';
import {PutEventsRequestEntry, PutEventsResponse} from 'aws-sdk/clients/eventbridge';
import {Errors, EventBridgeError, TeamsMessagesValues} from './errors';

export class Events {
  readonly eventSource: EventBridgeError.Sources;
  readonly params: {Entries: Array<Required<Pick<PutEventsRequestEntry, 'Source' | 'DetailType' | 'Detail'>>>};
  static readonly AWSEventBridge: AWS.EventBridge = new AWS.EventBridge({region: 'us-east-1'});

  constructor(
    eventSource: EventBridgeError.Sources,
    masterNode?: string,
  ) {
    this.eventSource = eventSource;
    this.params = {
      Entries: [
        {
          Source: `${this.eventSource}.environment`,
          Detail: JSON.stringify(
            {
              // environment: environment,
              // processStartTime: processStartTime,
              // masterNode: masterNode,
            },
          ),
          DetailType: EventBridgeError.DetailTypes.LambdaInvocation,
        },
      ],
    };
  }

  logEvent = (): void => {
    console.log(`\nExample Event:\n${JSON.stringify(this.params, null, 2)}`);
  };

  logDetails = (): void => {
    console.log(`\nExample Details of Event`);
    for (const entry of this.params.Entries) {
      console.log(JSON.stringify(JSON.parse(entry.Detail), null, 2));
    }
  };

  putEvents = async (): Promise<void> => {
    let eventBridgeError: EventBridgeError;
    this.logEvent();

    try {
      const {Entries, FailedEntryCount}: PutEventsResponse = await Events.AWSEventBridge.putEvents(this.params).promise();

      if (FailedEntryCount && FailedEntryCount > 0) {
        eventBridgeError = new EventBridgeError(
          {
            body: {
              errorType: Errors.ErrorTypes.EventBridge,
              errorMessage: EventBridgeError.ErrorMessages.PutEventsError,
              detailType: this.params.Entries[0].DetailType as EventBridgeError.DetailTypes,
              source: this.params.Entries[0].Source as unknown as EventBridgeError.Sources,
              teamsMessage: TeamsMessagesValues.EventBridgeError,
            },
          },
        );
      } else {
        console.log(`\nSuccessfully added events to EventBridge: [Source: ${this.eventSource}]\n${JSON.stringify(Entries, null, 2)}`);
        return;
      }
    } catch (error) {
      throw new EventBridgeError(
        {
          body: {
            errorType: Errors.ErrorTypes.EventBridge,
            errorMessage: EventBridgeError.ErrorMessages.PutEventsError,
            detailType: this.params.Entries[0].DetailType as EventBridgeError.DetailTypes,
            source: this.params.Entries[0].Source as unknown as EventBridgeError.Sources,
            teamsMessage: TeamsMessagesValues.EventBridgeError,
          },
        },
        error,
      );
    }

    throw eventBridgeError;
  };
}

export class CheckWARFilesEvent extends Events {
  static readonly processStartTime: string;

  constructor(
    eventSource: EventBridgeError.Sources,
  ) {
    super(eventSource);
  }
}

export class TriggerServerRestartEvent extends Events {
  static readonly processStartTime: string;

  constructor(
    eventSource: EventBridgeError.Sources,
    masterNode: string,
  ) {
    super(eventSource, masterNode);
  }
}