import * as AWS from 'aws-sdk/';
import {Body, BucketName, GetObjectAttributesOutput, GetObjectOutput, IsTruncated, ListObjectsV2Output, NextToken, Object, ObjectKey, ObjectList, ObjectSize, ObjectVersion, ObjectVersionId, ObjectVersionList, Prefix} from 'aws-sdk/clients/s3';
import {Errors, ProcessInfo, S3Error, TeamsMessagesValues} from './errors';
import {CommandResults} from './types';

// TODO: Initialize with name..?
// const S3Client: AWS.S3 = new AWS.S3();

// Types that require needed properties to be defined to make error handling easier
export type RequiredObjectList = Array<RequiredObject>;
export type RequiredObject = Required<Pick<Object, 'Key' | 'Size'>>;
export type RequiredGetObjectOutput = Required<Pick<GetObjectOutput, 'Body'>>;
export type RequiredGetObjectAttributesOutput = Required<Pick<GetObjectAttributesOutput, 'ObjectSize'>>;

/** Information about an S3 Bucket */
export class S3Bucket {
  /** The name of the S3 Bucket */
  readonly name: BucketName;
  /** The list of objects that are already initialized to help prevent duplicate API calls */
  trackedObjects: Array<S3Object> = [];
  /** The AWS S3 Client */
  static readonly S3Client: AWS.S3 = new AWS.S3();


  /**
   * Information about an S3 Bucket
   *
   * @param {BucketName} bucket The name of the S3 Bucket
   */
  constructor(
    bucket: BucketName,
  ) {
    console.count('Creating S3Bucket object');
    this.name = bucket;
  }

  /**
   * Adds an object whose API was called to the list of tracked objects in order to reuse the information and prevent duplicate calls to the API
   * @param {S3Object} object
   * @param {boolean} initialize
   * @return {Promise<void>}
   */
  addTrackedObject = async (object: S3Object, initialize: boolean = false): Promise<void> => {
    if (initialize) {
      await object.initialize();
    }
    this.trackedObjects.push(object);
  };

  /** Gets the S3 Object with the given key
   *
   * @param {S3.ObjectKey} key The key of the object to get
   * @param {boolean} initialize Whether or not to initialize the object with some API calls
   *
   * @return {Promise<S3Object>} The S3 Object with the given key
   */
  getObject = async (key: ObjectKey, initialize: boolean = false): Promise<S3Object> => {
    // Check if the object is already being tracked
    let foundObject: S3Object | undefined;
    for (const object of this.trackedObjects) {
      if (object.key === key) {
        foundObject = object;
        // Initialize the object with some API calls if information is desired to be added to the tracked object
        if (initialize) {
          await foundObject.initialize();
        }
        break;
      } // TODO: Else throw error?
    }

    // If the object was not found, create a new one and add it to the list of tracked objects
    if (!foundObject) {
      foundObject = new S3Object(this.name, key);
      await this.addTrackedObject(foundObject, initialize);
    }

    return foundObject;
  };

  /** Gets the S3 Object with the given key and version
   *
   * @param {Prefix} prefix The optional prefix of the object
   *
   * @return {Promise<RequiredObjectList>} The list of objects in the S3 Bucket
   */
  listObjects = async (prefix?: Prefix): Promise<RequiredObjectList> => {
    // Helper variables to determine what to do with the S3 data
    /** Whether the S3 Bucket has more Objects */
    let isTruncated: IsTruncated | undefined;
    /** The token to continue the ListObjects call if the S3 Bucket has more Objects */
    let continuationToken: string | undefined = undefined;
    /** All S3 Objects pulled from the ListObjects command */
    let allS3Objects: RequiredObjectList = [];

    // Gets all objects in the S3 Bucket with the given prefix if provided
    do {
      try {
        const {Contents, IsTruncated, NextContinuationToken}: Pick<ListObjectsV2Output, 'Contents' | 'IsTruncated' | 'NextContinuationToken'> = await S3Bucket.S3Client.listObjectsV2({Bucket: this.name, Prefix: prefix, ContinuationToken: continuationToken}).promise();

        // Information for looping through the S3 Bucket
        isTruncated = IsTruncated;
        continuationToken = NextContinuationToken;

        // Add the objects to the list of all S3 Objects
        //   Error will be thrown if Contents is undefined since it can't be cast to a RequiredObjectList
        allS3Objects = allS3Objects.concat(Contents as RequiredObjectList);
      } catch (error) {
        throw new S3Error(
          {
            body: {
              bucket: this.name,
              errorMessage: S3Error.ErrorMessages.ListObjectsError,
              errorType: Errors.ErrorTypes.S3,
              teamsMessage: TeamsMessagesValues.S3Error,
            },
          },
          error,
        );
      }
    } while (isTruncated);

    // Throw an error if there are no objects in the S3 Bucket and there is no S3 Error
    if (allS3Objects.length === 0) {
      throw new S3Error(
        {
          body: {
            bucket: this.name,
            errorMessage: S3Error.ErrorMessages.BucketContentsUndefined,
            errorType: Errors.ErrorTypes.S3,
            teamsMessage: TeamsMessagesValues.S3Error,
          },
        },
      );
    } else {
      // Log the objects in the S3 Bucket
      const nonLogObjects: RequiredObjectList = allS3Objects.filter((object: RequiredObject) => !object.Key.includes('SSM') && !object.Key.includes('errorLogs'));
      // Limits the logged objects to non log objects for conciseness
      if (nonLogObjects.length > 0) {
        console.log(`All Non-Log Objects in the S3 Bucket ${this.name} Displayed in the Table Below`);
        S3Bucket.logS3ContentsList(nonLogObjects);
      } else {
        // If there are only log objects, log all objects
        console.log(`All Objects in the S3 Bucket ${this.name}${prefix ? ` at ${prefix}` : ''} Displayed in the Table Below`);
        S3Bucket.logS3ContentsList(allS3Objects, true);
      }
      return allS3Objects;
    }
  };

  /** Logs the given list of S3 Objects
   *
   * @param {RequiredObjectList} contents The list of S3 Objects to log
   * @param {boolean} showOriginalFileStructure Whether or not to show the original file structure of the S3 Objects when trying to display log objects
   */
  static logS3ContentsList = (contents: RequiredObjectList, showOriginalFileStructure: boolean = false): void => {
    let contentsForLogs: RequiredObjectList;
    if (!showOriginalFileStructure) {
      // Parsing in order to remove reference to passed in object and use the raw value instead
      contentsForLogs = (JSON.parse(JSON.stringify(contents)) as RequiredObjectList).filter(content => !content.Key.includes('Original-File-Structure'));
    } else {
      // Parsing in order to remove reference to passed in object and use the raw value instead
      contentsForLogs = (JSON.parse(JSON.stringify(contents)) as RequiredObjectList);
    }

    // Log the objects in the S3 Bucket
    for (const content of contentsForLogs) {
      // If the key is too long, shorten it for logging
      if (content.Key!.length > 40) {
        const contentArray: Array<string> = content.Key!.split('/');
        // Random JSON.stringify to get rid of the TS error with Jest
        JSON.stringify(contentArray);
        // Gets the Instance ID if it exists in the S3 Path
        const instanceId: string | undefined = contentArray.find(key => key.startsWith('i-'));
        let rootKey: string;
        let fileKey: string;
        // Shortens the first and last keys in the S3 Path if they are too long for logs
        if (contentArray[0].length + contentArray[contentArray.length - 1].length > 40) {
          // Random JSON.stringify to get rid of the TS error with Jest
          JSON.stringify(contentArray);
          rootKey = `${contentArray[0].slice(0, 10)}...${contentArray[0].slice(contentArray[0].length - 10, contentArray[0].length)}`;
          fileKey = `${contentArray[contentArray.length - 1].slice(0, 10)}...${contentArray[contentArray.length - 1].slice(contentArray[contentArray.length - 1].length - 10, contentArray[contentArray.length - 1].length)}`;
        } else {
          // The first and last keys in the S3 Path are not too long for logs
          rootKey = contentArray[0];
          fileKey = contentArray[contentArray.length - 1];
        }

        // If the Instance ID is included in the S3 Path, include it in the logs path
        if (instanceId) {
          content.Key = `${rootKey}/.../${contentArray[contentArray.indexOf(instanceId) - 1]}/.../${fileKey}`;
        } else {
          content.Key = `${rootKey}/.../${fileKey}`;
        }
      }
    }

    console.table(contentsForLogs);
  };
}

/** Initializes an S3 Object from a file in an S3 Bucket with application information and the necessary properties needed from the S3 metadata */
export class S3Object {
  readonly bucket: BucketName;
  readonly key: ObjectKey;
  body?: Body;
  fileContents?: string;
  objectSize?: ObjectSize;
  newestVersion?: ObjectVersion;
  static readonly S3Client: AWS.S3 = new AWS.S3();

  /** Initializes an S3 Object from a file in an S3 Bucket with application information and the necessary properties needed from the S3 metadata
   *
   * @param {BucketName} bucket The name of the S3 Bucket
   * @param {ObjectKey} key The key of the S3 Object in the S3 Bucket
   */
  constructor(
    bucket: BucketName,
    key: ObjectKey,
  ) {
    this.bucket = bucket;
    this.key = key;
  }

  /** Sets the newest version, body, file contents, and object size of the S3 Object */
  initialize = async (): Promise<void> => {
    await this.setNewestVersion();
    await this.setBody(this.newestVersion?.VersionId);
    this.fileContents = this.body!.toString();
    this.objectSize = this.newestVersion?.Size;
    // await this.setObjectSize(this.newestVersion?.VersionId);
  };

  /**
   * Formats the S3 Object's file contents into a JSON object for logging
   *
   * The contents of a log file being read from should be formatted like:
   * ```
   * Key (Tenant)
   * Tenant's response status of command sent
   * Key (Tenant) 2
   * Tenant's response status of command sent 2
   * ```
   *
   * Which will be formatted into:
   * ```
   * {
   *  "Key (Tenant)": "Tenant's response status of command sent",
   *  "Key (Tenant) 2": "Tenant's response status of command sent 2"
   * }
   * ```
   *
   * @param {string} contents The contents of the S3 Object's file
   * @returns {{[key: string]: string}} The formatted contents of the S3 Object's file as a JSON object
   */
  public static formatContents = (contents: string): {[key: string]: string} => {
    // Removes the new line characters and replaces them with actual new lines, then splits the string into an array of strings
    const entries: Array<string> = JSON.stringify(contents)
                                       .replace(/\\n/g, '\n')
                                       .replace(/\\r/g, ' ')
                                       .replace(/"/g, '')
                                       .split(/\n/);
    // Random JSON.stringify to get rid of the TS error with Jest
    JSON.stringify(entries);

    // Creates an object from the array of strings
    let contentsAsObject: {[key: string]: string} = {};
    for (const entry of entries) {
      let split = entry.split(/: /, 2);
      if (split[0] !== '') {
        contentsAsObject[split[0]] = entry.slice(split[0].length + 2);
      }
    }

    return contentsAsObject;
  };

  /**
   * Gets the Body of an S3 Object or throws an error if the Body is empty
   *
   * @param versionId {string} The Version of the S3 Object
   *
   * @return {Promise<{Body: Body}>} The Body of the S3 Object
   */
  setBody = async (versionId?: ObjectVersionId): Promise<void> => {
    try {
      const {Body}: Pick<GetObjectOutput, 'Body'> = await S3Object.S3Client.getObject({Bucket: this.bucket, Key: this.key, VersionId: versionId}).promise();

      // Sets the Body of the S3 Object
      //   Error will be thrown if Body is undefined since it can't be cast to a RequiredObjectOutput
      this.body = Body as RequiredGetObjectOutput;
      return;
    } catch (error) {
      throw new S3Error(
        {
          body: {
            bucket: this.bucket,
            key: this.key,
            errorMessage: S3Error.ErrorMessages.GetObjectError,
            errorType: Errors.ErrorTypes.S3,
            teamsMessage: TeamsMessagesValues.S3Error,
          },
        },
        error,
      );
    }
  };

  /** Gets the Object Size of an S3 Object
   *
   * @param {ObjectVersionId} versionId Optional. The Version of the S3 Object
   * @return {Promise<void>}
   */
  setObjectSize = async (versionId?: ObjectVersionId): Promise<void> => {
    try {
      const {ObjectSize}: Pick<GetObjectAttributesOutput, 'ObjectSize'> = await S3Object.S3Client.getObjectAttributes({Bucket: this.bucket, Key: this.key, ObjectAttributes: ['ObjectSize'], VersionId: versionId}).promise();
      // Sets the Object Size of the S3 Object
      //   Error will be thrown if ObjectSize is undefined since it can't be cast to ObjectSize
      this.objectSize = ObjectSize as ObjectSize;
    } catch (error) {
      throw new S3Error(
        {
          body: {
            bucket: this.bucket,
            key: this.key,
            errorMessage: S3Error.ErrorMessages.ObjectAttributesError,
            errorType: Errors.ErrorTypes.S3,
            teamsMessage: TeamsMessagesValues.S3Error,
          },
        },
        error,
      );
    }
  };

  /**
   * Gets the Version of an S3 Object or throws an error if the Versions are empty
   *
   * @return {Promise<{Versions: ObjectVersion}>} The Version of the S3 Object
   */
  setNewestVersion = async (): Promise<void> => {
    try {
      const {Versions}: {Versions?: ObjectVersionList} = await S3Object.S3Client.listObjectVersions({Bucket: this.bucket, Prefix: this.key, MaxKeys: 1}).promise();
      if (Versions) {
        this.newestVersion = Versions[0];
      } else {
        new S3Error(
          {
            body: {
              bucket: this.bucket,
              key: this.key,
              errorMessage: S3Error.ErrorMessages.ObjectVersionUndefined,
              errorType: Errors.ErrorTypes.S3,
              teamsMessage: TeamsMessagesValues.S3Error,
            },
          },
        );
      }
    } catch (error) {
      new S3Error(
        {
          body: {
            bucket: this.bucket,
            key: this.key,
            errorMessage: S3Error.ErrorMessages.ObjectVersionUndefined,
            errorType: Errors.ErrorTypes.S3,
            teamsMessage: TeamsMessagesValues.S3Error,
          },
        },
        error,
      );
    }
    // console.log('There are no object versions available. Continuing with the process..');
  };

  replaceObject = async (body: Body): Promise<void> => {
    try {
      await S3Object.S3Client.putObject({Bucket: this.bucket, Key: this.key, Body: body as Buffer}).promise();
      this.body = body;
      console.log(`\nSuccessfully put a file in the S3 Bucket ${this.bucket} at ${this.key}`);
    } catch (error) {
      throw new S3Error(
        {
          body: {
            bucket: this.bucket,
            key: this.key,
            errorMessage: S3Error.ErrorMessages.UploadError,
            errorType: Errors.ErrorTypes.S3,
            teamsMessage: TeamsMessagesValues.S3Error,
          },
        },
        error,
      );
    }
  };

  // TODO: Maybe return new tracked object..?
  copyObject = async (newPrefix: ObjectKey, newBucket?: BucketName): Promise<void> => {
    const destinationBucket: BucketName = newBucket ?? this.bucket;

    try {
      await S3Object.S3Client.copyObject({Bucket: destinationBucket, Key: newPrefix, CopySource: `${this.bucket}/${this.key}`}).promise();
    } catch (error) {
      throw new S3Error(
        {
          body: {
            bucket: this.bucket,
            key: this.key,
            errorMessage: S3Error.ErrorMessages.CopyObjectError,
            errorType: Errors.ErrorTypes.S3,
            teamsMessage: TeamsMessagesValues.S3Error,
          },
        },
        error,
      );
    }
  };
}

export class S3 {
  static newBucket: S3Bucket;
  static oldBucket: S3Bucket;
  static readonly S3Client: AWS.S3 = new AWS.S3();

  constructor(
    newBucket: BucketName,
    oldBucket: BucketName,
  ) {
    S3.newBucket = new S3Bucket(newBucket);
    S3.oldBucket = new S3Bucket(oldBucket);
  }

  deleteObject = async (bucket: BucketName, key: ObjectKey, info: ProcessInfo) => {
    try {
      await S3.S3Client.deleteObject({Bucket: bucket, Key: key}).promise();
    } catch (error) {
      throw new S3Error(
        {
          body: {
            bucket: bucket,
            key: key,
            errorMessage: S3Error.ErrorMessages.DeleteObjectError,
            errorType: Errors.ErrorTypes.S3,
            teamsMessage: TeamsMessagesValues.S3Error,
          },
        },
        error,
      );
    }
  };

  copyLogsToNewLocation = async (prefix: Prefix): Promise<{Contents: ObjectList}> => {
    const Contents: ObjectList = await S3.newBucket.listObjects(prefix);

    console.log('Found all contents here: ', Contents);
    const parsedS3Key: Array<string> = prefix.split('/');
    parsedS3Key.pop();
    let copiedObjects: Array<{source: string, destination: string, destinationPrefix: string}> = [];
    for (const object of Contents) {
      const parsedObjectKey: Array<string> = object.Key!.split('/')!; // TODO: Avoid using ! operator if possible
      const instanceId: string = parsedObjectKey.find((value: string) => value.startsWith('i-'))!; // TODO: Avoid using ! operator if possible
      const destinationPrefix: string = `${parsedS3Key.join('/')}/${instanceId}/${parsedObjectKey[parsedObjectKey.length - 1]}`;
      copiedObjects.push(
        {
          source: `Original-File-Structure/.../${object.Key!.split('/')[object.Key!.split('/').length - 1]}`,
          destination: `${parsedObjectKey[parsedObjectKey.indexOf(instanceId) - 1]}/${instanceId}/${parsedObjectKey[parsedObjectKey.length - 1]}`,
          destinationPrefix: destinationPrefix,
        },
      );
      await (await S3.newBucket.getObject(object.Key!)).copyObject(destinationPrefix, S3.newBucket.name);
    }

    console.log('Copied Objects Displayed in the Table Below');
    console.table(copiedObjects);
    return {Contents: Contents};
  };

// TODO: Add documentation on manually adding permissions to kbs-temp-mdm-cicd-${environment} S3Client Bucket so that this lambda can access it
// TODO: Handle freak Teams Post error..? https://us-east-1.console.aws.amazon.com/cloudwatch/home?region=us-east-1#logsV2:log-groups/log-group/$252Faws$252Flambda$252FProjectFerrariCheckWARFilesLambdaDEV2/log-events/2023$252F05$252F09$252F$255B$2524LATEST$255D406c80c445f546fbab3b3981e62ae107
}

export const getCommandResults = async (s3: S3, contents: ObjectList, master: boolean = false): Promise<CommandResults> => {
  let commandResults: CommandResults = {};

  for (const content of contents) {
    if (content.Key) {
      const instanceId: string = content.Key.split('/').find((value: string) => value.startsWith('i-'))!;

      if (content.Key.includes('stdout')) {
        try {
          if (!Object.keys(commandResults).includes(instanceId)) {
            commandResults[instanceId] = {};
          }
        } catch (error) {
          commandResults[instanceId] = {};
        }

        commandResults[instanceId].stdout = (await S3.newBucket.getObject(content.Key, true)).fileContents;

        if (master) {
          commandResults[instanceId].master = master;
        } else {
          commandResults[instanceId].master = commandResults[instanceId].stdout!.includes('master');
        }

      } else if (content.Key.includes('stderr')) {
        try {
          if (!Object.keys(commandResults).includes(instanceId)) {
            commandResults[instanceId] = {};
          }
        } catch (error) {
          commandResults[instanceId] = {};
        }
        commandResults[instanceId].stderr = (await S3.newBucket.getObject(content.Key, true)).fileContents;
      }
    } else {
      throw new S3Error(
        {
          body: {
            bucket: S3.newBucket.name,
            errorType: Errors.ErrorTypes.S3,
            errorMessage: S3Error.ErrorMessages.ObjectKeyUndefined,
            teamsMessage: TeamsMessagesValues.SSMError,
          },
        },
      );
    }
  }

  console.log(`Command Results: ${JSON.stringify(commandResults, null, 2)}`);

  return commandResults;
};