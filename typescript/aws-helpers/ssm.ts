import * as AWS from 'aws-sdk';
import {CommandId, CommandStatus, GetParameterRequest, GetParameterResult, ListCommandsResult, ParameterValue, ParameterValueList, SendCommandResult} from 'aws-sdk/clients/ssm';
import {Errors, SSMError, TeamsMessagesValues} from './errors';

export interface SSMConfig {
  readonly newBucket: string;
  readonly environment: string;
  readonly processStartTime: string;
}

export class SSM {
  readonly nodes: Array<string>;
  static readonly SSMClient: AWS.SSM = new AWS.SSM({region: 'us-east-1'});
  config: SSMConfig;

  constructor(
    nodes: Array<string>,
    ssmConfig: SSMConfig,
  ) {
    this.nodes = nodes;
    this.config = ssmConfig;
  }

  sendCommand = async (commands: ParameterValueList, commandsForLogs: ParameterValueList, commandType: string, document: string, nodes?: Array<string>): Promise<{commandId: CommandId, s3Key: string}> => {
    // TODO: Get examples of unauthorized, invalid, and success responses for all commands sent to add to documentation
    console.log(`\nSending the following Commands to the EC2 Instances: ${nodes ?? this.nodes}`);
    let tableLogs: Array<string> = [];
    for (let command of commandsForLogs) {
      while (command.length > 150) {
        tableLogs.push(command.slice(0, 151));
        command = command.slice(151);
      }

      tableLogs.push(command);
    }
    console.table(tableLogs);

    let ssmError: SSMError;
    try {
      const {Command}: SendCommandResult = await SSM.SSMClient.sendCommand(
        {
          DocumentName: document,
          // TODO: Implement Comment to make command history easier to find
          Comment: `${commandType}: ${this.config.environment}`,
          Parameters: {
            commands: commands,
          },
          InstanceIds: nodes ?? this.nodes,
          OutputS3BucketName: this.config.newBucket,
          OutputS3KeyPrefix: `SSM-Outputs/${this.config.processStartTime}/${commandType}/Original-File-Structure`,
          CloudWatchOutputConfig: {
            CloudWatchOutputEnabled: true,
            CloudWatchLogGroupName: `ProjectFerrariLogs${this.config.environment}`,
          },
          // NotificationConfig: {} // TODO: Research this! :)
          // TimeoutSeconds: 7200, // TODO: Is the timeout necessary?
        },
      ).promise();

      console.log('Command in trigger restart lambda in sendCommand');
      console.log(Command);

      if (Command && Command.CommandId && Command.OutputS3KeyPrefix) {
        return {commandId: Command.CommandId, s3Key: `${Command.OutputS3KeyPrefix}`}; // TODO: Change to an array for when there are 2 instances
      } else {
        ssmError = new SSMError(
          {
            body: {
              commands: commandsForLogs,
              errorType: Errors.ErrorTypes.SSM,
              errorMessage: SSMError.ErrorMessages.SendCommandError,
              teamsMessage: TeamsMessagesValues.SSMError,
            },
          },
        );
      }
    } catch (error) {
      throw new SSMError(
        {
          body: {
            commands: commands,
            errorType: Errors.ErrorTypes.SSM,
            errorMessage: SSMError.ErrorMessages.SendCommandError,
            teamsMessage: TeamsMessagesValues.SSMError,
          },
        },
        error,
      );
    }

    throw ssmError;
  };

  waitForCommand = async (commandId: CommandId, retryRate: number): Promise<{commandStatus: CommandStatus}> => {
    // SSMError.getConnectionStatus() // TODO: Research this! :)
    let ssmError: SSMError | undefined = undefined;
    let timeTaken: number = 0;
    let commandStatus: CommandStatus | undefined;
    do {
      console.log(`Still Waiting on Command to Finish. Total Time: ${(timeTaken * retryRate) / 1000}`);
      timeTaken++;
      try {
        const {Commands}: ListCommandsResult = await SSM.SSMClient.listCommands({CommandId: commandId}).promise();
        if (Commands) {
          if (Commands.length === 1) {
            commandStatus = Commands[0].Status;
            if (commandStatus === 'InProgress' || commandStatus === 'Pending') {
              await new Promise(resolve => setTimeout(resolve, retryRate));
            }
          } else {
            if (Commands.length === 0) {
              ssmError = new SSMError(
                {
                  body: {
                    errorType: Errors.ErrorTypes.SSM,
                    errorMessage: SSMError.ErrorMessages.NoCommandError,
                    commands: Commands,
                    teamsMessage: TeamsMessagesValues.SSMError,
                    commandId: commandId,
                  },
                },
              );
            } else {
              ssmError = new SSMError(
                {
                  body: {
                    commandId: commandId,
                    errorType: Errors.ErrorTypes.SSM,
                    errorMessage: SSMError.ErrorMessages.MoreThanOneCommandError,
                    commands: Commands,
                    teamsMessage: TeamsMessagesValues.SSMError,
                  },
                },
              );
            }
          }
        } else {
          ssmError = new SSMError(
            {
              body: {
                commandId: commandId,
                errorType: Errors.ErrorTypes.SSM,
                errorMessage: SSMError.ErrorMessages.NoCommandError,
                commands: Commands,
                teamsMessage: TeamsMessagesValues.SSMError,
              },
            },
          );
        }
      } catch (error) {
        throw new SSMError(
          {
            body: {
              commandId: commandId,
              errorType: Errors.ErrorTypes.SSM,
              errorMessage: SSMError.ErrorMessages.ListCommandsError,
              teamsMessage: TeamsMessagesValues.SSMError,
            },
          },
          error,
        );
      }
    } while (commandStatus === 'InProgress' || commandStatus === 'Pending');

    console.log(`\nCommand Finished\nApproximate Time Taken to Complete: ${(timeTaken * retryRate
                                                                           ) / 1000}`);

    if (ssmError) {
      throw ssmError;
    } else {
      return {
        commandStatus: commandStatus!, // TODO: See if can avoid ! operator
      };
    }
  };

  // TODO: The most secure way to do this and to avoid this being logged (Even on AWS SSM in 'Run Command') would be to call the AWS CLI to get this parameter from within the EC2
  //  And utilize stdout / stdin
  getSSMParameter = async (params: GetParameterRequest): Promise<ParameterValue> => {
    let ssmError: SSMError;
    try {
      const {Parameter}: GetParameterResult = await SSM.SSMClient.getParameter(params).promise();
      if (Parameter && Parameter.Value) {
        return Parameter.Value;
      } else {
        ssmError = new SSMError(
          {
            body: {
              parameter: params.Name,
              errorType: Errors.ErrorTypes.SSM,
              errorMessage: SSMError.ErrorMessages.GetSSMParameterError,
              teamsMessage: TeamsMessagesValues.SSMError,
            },
          },
        );
      }
    } catch (error) {
      ssmError = new SSMError(
        {
          body: {
            parameter: params.Name,
            errorType: Errors.ErrorTypes.SSM,
            errorMessage: SSMError.ErrorMessages.GetSSMParameterError,
            teamsMessage: TeamsMessagesValues.SSMError,
          },
        },
        error,
      );
    }

    throw ssmError;
  };
}