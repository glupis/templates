import {Context, EventBridgeEvent} from 'aws-lambda';
import {BucketName, ObjectKey, ObjectList} from 'aws-sdk/clients/s3';
import {CommandId, CommandList, ParameterName, ParameterValueList} from 'aws-sdk/clients/ssm';
import axios, {AxiosError} from 'axios';

export enum StatusCodes {
  Success = 200,
  Error = 400,
}

export interface ProcessInfo {
  // Insert Process Info Properties Here
  // Example:
  // environment: Environment;
  // processStartTime: string;
}

//-------------------------- EventBridge Events --------------------------//

export type DetailedEventBridgeEvent = EventBridgeEvent<string, DetailedEvent /* | OtherEvent */>;
export type BaseEventBridgeEvent = Pick<EventBridgeEvent<string, BaseEvent>, 'detail'>;

export interface BaseEvent {
  // Insert Base Event Properties Here
  // Example:
  // environment: Environment,
}

export interface DetailedEvent extends BaseEvent {
  // Insert Event Properties Here
  // Example:
  // processStartTime: string,
}

//-------------------------- Success Responses --------------------------//

export type ResponseBodies = ResponseBodyTemplate; // | AnotherResponseBody

interface BaseBody {
  // Insert Base Body Properties Here
  // Example:
  // environment: Environment,
}

export interface ResponseBodyTemplate extends BaseBody {
  // Insert Body Properties Here
}

interface SuccessResponse<T = ResponseBodies> {
  statusCode: StatusCodes.Success,
  body: T,
}

export type Response<T> = SuccessResponse<T>;

//-------------------------- Error Responses --------------------------//
export type ErrorResponseBodies = ErrorResponseBodyTemplate; // | AnotherErrorResponseBody

interface BaseErrorBody extends BaseBody {
  errorType: Errors.ErrorTypes,
  errorMessage: string,
  teamsMessage?: TeamsMessagesValues,
}

export type ErrorBody<T = Errors.ErrorInfo> = BaseErrorBody & T;

export interface ErrorResponseBodyTemplate extends BaseErrorBody {
  // Insert Body Properties Here
}

export type ErrorResponses<T> = ErrorResponse<T>;

interface ErrorResponse<T = ErrorResponseBodies> {
}

interface ErrorResponse {
  statusCode: StatusCodes.Error,
  body: ErrorBody,
  error: unknown,
}

export type ErrorInfo<T = Errors.ErrorInfo> = {
  body: ErrorBody<T>,
};

//-------------------------- Errors --------------------------//

export abstract class Errors<T = ResponseBodies> extends Error {
  readonly response: ErrorResponse<T>;
  readonly errorInfo: ErrorBody<T>;
  abstract readonly errorMessage: string;


  protected constructor(
    errorInfo: ErrorInfo<T>,
    error: unknown,
  ) {
    if (error instanceof Error) {
      super(`${error.message}: ${errorInfo.body.errorMessage}`);
    } else {
      super(errorInfo.body.errorMessage);
    }

    this.errorInfo = {
      ...errorInfo.body,
    };

    this.name = errorInfo.body.errorType;

    error = this.createError(errorInfo.body.errorMessage, error);

    this.response = {
      statusCode: 400,
      body: {
        ...errorInfo.body,
        errorMessage: this.buildErrorMessage(errorInfo.body, error instanceof Error ? error.message : undefined),
      },
      error: error,
    };

    this.logError(error);
  }

  logError(error?: Error | unknown): void {
    const errorMessage: string = (error instanceof Error || error instanceof Errors
                                 )
                                 ? error.message
                                 : 'Error Getting Error Message';

    const errorStack: string | undefined = (error instanceof Error || error instanceof Errors
                                           )
                                           ? error.stack
                                           : 'Error Getting Error Stack';
    console.log(
      `\n${this.response.body.errorType} Error: ${this.response.body.errorMessage}\n\n` +
      `Error:` +
      `\n${JSON.stringify(errorStack)}`,
    );
  }

  createError(errorMessage: string, error?: unknown): Error | unknown {
    return (error instanceof Error || error instanceof Errors
           ) ? error
             : new Error(errorMessage);
  }

  abstract buildErrorMessage(errorInfo: ErrorBody, errorMessage?: string): string;

}

export namespace Errors {
  export enum ErrorTypes {
    S3 = 'S3',
    EventBridge = 'EventBridge',
    SSM = 'SSM',
    EC2 = 'EC2',
    Unknown = 'Unknown',
    Teams = 'Teams',
  }

  export type ErrorInfo = S3Error.ErrorInfo | SSMError.ErrorInfo | EventBridgeError.ErrorInfo | TeamsPostError.ErrorInfo;
}

//-------------------------- S3 Errors --------------------------//

export class S3Error<T = ResponseBodies> extends Errors {
  readonly errorMessage: string;

  constructor(
    errorInfo: ErrorInfo<S3Error.ErrorInfo>,
    error?: unknown,
  ) {
    super(
      {
        ...errorInfo,
      },
      error,
    );

    this.errorMessage = this.buildErrorMessage(errorInfo.body);
  }

  buildErrorMessage(errorInfo: ErrorBody<S3Error.ErrorInfo>): string {
    return `${errorInfo.bucket}${errorInfo.key ? `/${errorInfo.key}` : ''}: ${errorInfo.errorMessage}`;
  }
}

export namespace S3Error {
  export enum ErrorMessages {
    ListObjectsError = 'Failure Listing the Objects in the S3 Bucket', // Unable to retrieve the S3Error Bucket's Contents
    GetObjectError = 'Failure Getting the Object from the S3 Bucket', // Unable to retrieve the S3Error Bucket's Contents
    UploadError = 'Failure Uploading the File to the S3 Bucket', // Unable to upload the file to S3Error
    SearchError = 'Unable to find any results for search string', // Unable to find any results for search string
    ObjectAttributesError = 'Failure Getting the Object Attributes for the S3 Bucket', // Failure Getting Object Attributes from S3Error // Failure Obtaining the Key and Size of the S3Error Object
    CopyObjectError = 'Failure Copying the Object from the S3 Bucket', // Failure Getting Object Attributes from S3Error // Failure Obtaining the Key and Size of the S3Error Object
    DeleteObjectError = 'Failure Deleting the Object from the S3 Bucket', // Failure Getting Object Attributes from S3Error // Failure Obtaining the Key and Size of the S3Error Object
    // Undefined Errors
    BucketContentsUndefined = 'The Contents of the S3 Bucket are Undefined',
    ObjectBodyUndefined = 'The Body of the Object in the S3 Bucket is Undefined',
    ObjectKeyUndefined = 'The Key of the Object in the S3 Bucket is Undefined',
    ObjectSizeUndefined = 'The Size of the Object in the S3 Bucket is Undefined',
    ObjectVersionUndefined = 'The Versions of the Object in the S3 Bucket are Undefined', // Failure Getting the Object Version from S3Error
    // UndefinedError = S3UndefinedErrors,
    ObjectSizeTooSmall = 'The Size of the Object in the S3 Bucket is Less Than 1MB - There Was Probably a Problem Building the WAR File',
  }

  export type ErrorInfo = {
    errorMessage: ErrorMessages,
    bucket: BucketName,
    key?: ObjectKey,
  };
}

//-------------------------- SSM Errors --------------------------//

export class SSMError extends Errors<ResponseBodies> {
  readonly errorMessage: string;

  constructor(
    errorInfo: ErrorInfo<SSMError.ErrorInfo>,
    error?: unknown,
  ) {
    super(
      {
        ...errorInfo,
      },
      error,
    );

    this.errorMessage = this.buildErrorMessage(errorInfo.body);
  }

  buildErrorMessage(errorInfo: ErrorBody<SSMError.ErrorInfo>): string {
    return `${errorInfo.commandId}: ${errorInfo.errorMessage}${errorInfo.parameter ? ` | Parameter: ${errorInfo.parameter}` : ''}${errorInfo.commands ? `\nCommands:\n${errorInfo.commands}` : ''}`;
  }
}

export namespace SSMError {
  export enum ErrorMessages {
    GetSSMParameterError = 'Error obtaining the SSM Parameter',
    SendCommandError = 'Failure Sending the Command to AWS SSM',
    NoCommandError = 'There is no command running with this ID',
    MoreThanOneCommandError = 'There is more than one command with this ID',
    ListCommandsError = 'Error waiting for the command to finish execution',
    CommandsSentFailedError = 'The commands sent to AWS SSM failed',
  }

  export type ErrorInfo = {
    errorMessage: ErrorMessages,
    commands?: ParameterValueList | CommandList,
    commandId?: CommandId,
    parameter?: ParameterName,
  };
}

//-------------------------- EventBridge Errors --------------------------//

export class EventBridgeError extends Errors<ResponseBodies> {
  readonly errorMessage: string;

  constructor(
    errorInfo: ErrorInfo<EventBridgeError.ErrorInfo>,
    error?: unknown,
  ) {
    super(
      {
        ...errorInfo,
      },
      error,
    );

    this.errorMessage = this.buildErrorMessage(errorInfo.body);
  }

  buildErrorMessage(errorInfo: ErrorBody<EventBridgeError.ErrorInfo>): string {
    return `${errorInfo.errorMessage} | Source: ${errorInfo.source} | DetailType: ${errorInfo.detailType}`;
  }

}

export namespace EventBridgeError {
  export enum ErrorMessages {
    PutEventsError = 'Failure Adding Events to EventBridge'
  }

  export type ErrorInfo = {
    errorMessage: ErrorMessages,
    source: Sources,
    detailType: DetailTypes,
  };

  export enum Sources {
    // Insert Sources Here
    // Example:
    // FailurePostingTeams = 'project.teams.post.failure',
  }

  export enum DetailTypes {
    // Insert Detail Types Here
    // Example:
    LambdaInvocation = 'Lambda Invocation',
  }
}

//-------------------------- Teams Errors --------------------------//

export class TeamsPostError extends Errors<ResponseBodies> {
  readonly errorMessage: string;

  constructor(
    errorInfo: ErrorInfo<TeamsPostError.ErrorInfo>,
    error?: unknown,
  ) {
    super(
      {
        ...errorInfo,
      },
      error,
    );
    this.errorMessage = this.buildErrorMessage(errorInfo.body);
  }

  /**
   *
   * @param {ErrorBody<TeamsPostError.ErrorInfo>} errorInfo
   * @return {string}
   */
  buildErrorMessage(errorInfo: ErrorBody<TeamsPostError.ErrorInfo>): string {
    return errorInfo.errorMessage;
  }
}

export namespace TeamsPostError {
  export enum ErrorMessages {
    FailurePosting = 'Failure Posting to Teams'
  }

  export type ErrorInfo = {};
}

//------------------------- Teams Messages -------------------------//

export enum TeamsMessagesValues {
  EventBridgeError = 'An Error Occurred While Putting Events to AWS EventBridge',
  S3Error = 'An Error Occurred While Executing an S3 Command',
  SSMError = 'An Error Occurred While Executing an SSM Command',
  UnknownError = 'An Unknown Error Occurred',
}

export class TeamsMessage {
  readonly date: string;
  readonly message: TeamsMessagesValues | string;
  readonly sections: Array<TeamsSection>;
  readonly potentialActions: Array<TeamsPotentialAction>;
  readonly facts: Array<{name: string, value: string}>;
  readonly awsServiceImage: string;
  static lambdaURL: string;
  static logGroupURL: string;
  static logStreamURL: string;
  static functionName: string;
  readonly errorInfo: {errorType: string, errorMessage: string, errorStack: string} | undefined;

  constructor(
    message: TeamsMessagesValues,
    lambdaContext: Context,
    params?: {
      facts?: Array<{name: string, value: string}>,
      errorInfo?: {errorType: string, errorMessage: string, errorStack: string},
    },
  ) {

    TeamsMessage.setProcessInfo(lambdaContext);
    this.message = message;
    const now: Date = new Date();

    this.date = `${now.toDateString()} at ${now.toTimeString().split(' ')[0]}`;
    // TODO: Throw error if the static values are undefined?
    this.facts = [
      {
        'name': 'Started',
        'value': `${this.date} GMT`,
      },
    ].concat(params?.facts ?? []);
    this.errorInfo = params?.errorInfo;


    switch (message) {
      case TeamsMessagesValues.EventBridgeError: {
        this.awsServiceImage = "path/to/image";
      }
        break;
      case TeamsMessagesValues.S3Error: {
        this.awsServiceImage = "path/to/image";
      }
        break;
      case TeamsMessagesValues.SSMError: {
        this.awsServiceImage = "path/to/image";
      }
        break;
      case TeamsMessagesValues.UnknownError: {
        this.awsServiceImage = "path/to/image";
      }
        break;
    }

    this.potentialActions = [
      {
        '@type': 'OpenUri',
        name: `View Latest Logs`,
        targets: [
          {
            os: 'default',
            uri: TeamsMessage.logStreamURL,
          },
        ],
      },
      {
        '@type': 'OpenUri',
        name: `View All Logs`,
        targets: [
          {
            os: 'default',
            uri: TeamsMessage.logGroupURL,
          },
        ],
      },
      {
        '@type': 'OpenUri',
        name: `View the Lambda`,
        targets: [
          {
            os: 'default',
            uri: TeamsMessage.lambdaURL,
          },
        ],
      },
    ];

    this.sections = [
      {
        activityTitle: `Environment`, // ${this.environment} ?
        activitySubtitle: message,
        activityImage: this.awsServiceImage,
        facts: this.facts,
      },
    ];

    if (params?.errorInfo) {
      this.sections = this.sections.concat(
        [
          {
            activityTitle: 'Error Information',
            activitySubtitle: params.errorInfo.errorStack,
            activityImage: 'path/to/image',
            facts: [
              {
                name: 'Error Type',
                value: params.errorInfo.errorType,
              },
              {
                name: 'Error Message',
                value: params.errorInfo.errorMessage,
              },
            ],
          },
        ],
      );
    }
  }

  public static formatFacts = (facts: {[key: string]: string}): Array<{name: string, value: string}> => {
    let newFacts: Array<{name: string, value: string}> = [];
    for (const entry of Object.entries(facts)) {
      newFacts.push({name: entry[0], value: entry[1]});
    }

    return newFacts;
  };

  public static formatFactsPerCategory = (facts: {[key: string]: {[key: string]: string}}): Array<{name: string, value: string}> => {
    let newFacts: Array<{name: string, value: string}> = [];
    for (const fact of Object.entries(facts)) {
      for (const entry of Object.entries(fact[1])) {
        newFacts.push({name: `${fact[0]} | ${entry[0]}`, value: entry[1]});
      }
    }

    return newFacts;
  };

  public static setProcessInfo = (context: Context): void => {
    const {invokedFunctionArn, logGroupName, logStreamName, functionName}: {invokedFunctionArn: string, logGroupName: string, logStreamName: string, functionName: string} = context;
    const region: string = invokedFunctionArn.split(':')[3];
    const logGroup: string = logGroupName.replace(/\//g, '$252F');
    const logStream: string = logStreamName
      .replace('$', '$2524')
      .replace(/\//g, '$252F')
      .replace(/\[/g, '$255B')
      .replace(/]/g, '$255D');

    this.functionName = functionName;
    this.lambdaURL = 'basic-link-to-lambda'.replace(/\${REGION}/g, region).replace(/\${FUNCTION_NAME}/g, functionName);
    this.logGroupURL = 'basic-link-to-cloudwatch-log-group'.replace(/\${REGION}/g, region).replace(/\${LOG_GROUP}/g, logGroup);
    this.logStreamURL = 'basic-link-to-cloudwatch-log-stream'.replace(/\${REGION}/g, region).replace(/\${LOG_GROUP}/g, logGroup).replace(/\${LOG_STREAM}/g, logStream);
  };

  postToTeams = async (): Promise<void> => {
    try {
      console.log(`Posting to Teams: ${this.message}`);

      let timeout: NodeJS.Timeout | undefined;
      const timeoutPromise = new Promise((resolve, reject) => {
        timeout = setTimeout(() => {
          console.log('Posting to Teams has not seemed to generate a response. Hopefully it posted, but if not good luck troubleshooting ¯\\_(ツ)_/¯');
          resolve('Posting to Teams is taking longer than expected');
        }, 60000);
      });

      let post = new Promise((resolve, reject) => {
                               axios.post(
                                 'teams-url', // Example: CONSTANTS.ENVIRONMENTS[this.environment].WEBHOOKS.TEST_TEAMS,
                                 {
                                   '@type': 'MessageCard',
                                   '@context': 'http://schema.org/extensions',
                                   summary: this.message,
                                   sections: this.sections,
                                   potentialAction: this.potentialActions,
                                 },
                               ).then((response) => {
                                        resolve(response);
                                      },
                               );
                             },
      );

      await Promise.race(
        [
          post,
          timeoutPromise,
        ],
      );

      if (timeout) {
        clearTimeout(timeout);
      }

      console.log('Posted to Teams');

    } catch (error) {
      console.error(
        `Error Posting to Teams:\n` +
        `  ${(error as AxiosError).message}`,
      );
      // TODO: Figure out how to decouple this error from the rest
      throw new TeamsPostError(
        {
          body: {
            errorMessage: TeamsPostError.ErrorMessages.FailurePosting,
            errorType: Errors.ErrorTypes.Teams,
          },
        },
        error,
      );
    }
  };
}

interface TeamsSection {
  activityTitle: string,
  activitySubtitle: string,
  activityImage: string,
  facts: Array<{name: string, value: string}>,
}

type TeamsPotentialAction = TeamsOpenUri | TeamsActionCard;

type TeamsActionTypes = 'OpenUri' | 'ActionCard';

interface TeamsBaseActions {
  '@type': TeamsActionTypes,
  name: string,
}

interface TeamsOpenUri extends TeamsBaseActions {
  '@type': 'OpenUri',
  targets: Array<{os: 'default', uri: string}>,
}

interface TeamsActionCard extends TeamsBaseActions {
  '@type': 'ActionCard',
  inputs: Array<TeamsInput>,
  actions: Array<TeamsAction>
}

interface TeamsInput {
  '@type': 'TextInput',
  title: string,
  id: string,
  isMultiline: boolean,
}

interface TeamsAction {
  '@type': 'HttpPOST',
  name: string,
  target: string,
}