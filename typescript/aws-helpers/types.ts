// TODO: Sort out all the types so that errors and types are separate with no circular dependencies

export type Environment = 'PROD' | 'DEV';
export type TestEvents = 'TestEvents';

export interface CommandResults {
  [key: string]: {
    master?: boolean,
    stdout?: string,
    stderr?: string,
  },
}

export type ProcessURLs = {lambdaURL: string, logGroupURL: string, logStreamURL: string};