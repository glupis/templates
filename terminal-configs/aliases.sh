# Some good standards, which are not used if the user
# creates his/her own .bashrc/.bash_profile
#if [ -z "$OS" ]; then
#  (export MSYS="winsymlinks:nativestrict" && setx "MSYS" "winsymlinks:nativestrict") > /dev/null
#fi

# Built in from GitBash!
case "$TERM" in
  xterm*)
    # The following programs are known to require a Win32 Console
    # for interactive usage, therefore let's launch them through winpty
    # when run inside `mintty`.
    for name in node ipython php php5 psql python2.7; do
      case "$(type -p "$name".exe 2> /dev/null)" in
        '' | /usr/bin/*) continue ;;
      esac
      # Suppress this because we need it to expand when defined, not when used
      # shellcheck disable=SC2139
      alias $name="winpty $name.exe"
    done
    ;;
esac

# Bash Styling to Easily Make Things Pretty :)
bash_styling() {
  declare -A BASH_STYLING=(
    [reset_color]="\e[39m"                 # Reset Only Color
    [yellow]="\e[33m"                      # Yellow
    [bold_yellow]="\e[1m\e[33m"            # Bold Yellow
    [orange]="\e[38;5;208m"                # Orange
    [bold_orange]="\e[1m\e[38;5;208m"      # Bold Orange
    [red]="\e[38;5;196m"                   # Red
    [bold_red]="\e[1m\e[38;5;196m"         # Bold Red
    [blue]="\e[38;5;27m"                   # Blue
    [bold_blue]="\e[1m\e[38;5;27m"         # Bold Blue
    [light_blue]="\e[38;5;39m"             # Light Blue
    [bold_light_blue]="\e[1m\e[38;5;39m"   # Bold Light Blue
    [pink]="\e[38;5;177m"                  # Pink
    [bold_pink]="\e[1m\e[38;5;177m"        # Bold Pink
    [purple]="\e[38;5;129m"                # Purple
    [bold_purple]="\e[1m\e[38;5;129m"      # Bold Purple
    [light_purple]="\e[38;5;99m"           # Light Purple
    [bold_light_purple]="\e[1m\e[38;5;99m" # Bold Light Purple
    [green]="\e[32m"                       # Green
    [bold_green]="\e[1m\e[32m"             # Bold Green
    [checkmark]="\e[0m\e[1m\e[32m✓\e[0m"   # Reset All Bold Green ✓ Reset All
    [reset_all]="\e[0m"                    # Reset All Styling
    [bold]="\e[1m"                         # Bold
    [x]="\e[0m\e[1m\e[38;5;196mx\e[0m"     # Reset All Bold Red x Reset All
  )

  echo "${BASH_STYLING[$1]}"
}

# Opening Lines for the Terminal :)
initiate_new_session() {
  printf "%bUpdating Aliases..\n" "$(bash_styling "bold_green")"
  printf "  %bThe %bBuilding the Latest Edits of Your CLI%b Feature on new session is %b\n" \
    "$(bash_styling "reset_all")" \
    "$(bash_styling "bold_yellow")" \
    "$(bash_styling "reset_all")" \
    "$(
      if [ "$ENABLE_BUILD" == "ENABLED" ]; then
        printf "%b%s" "$(bash_styling "bold_green")" "enabled"
      else
        printf "%b%s" "$(bash_styling "bold_red")" "disabled"
      fi
    )"

  printf "  %bRun %bsetenv %s\n\n" \
    "$(bash_styling "reset_all")" \
    "$(bash_styling "bold_light_blue")" \
    "$(
      if [ "$ENABLE_BUILD" == "ENABLED" ]; then
        printf "\"DISABLED\"%b to disable this feature" "$(bash_styling "reset_all")"
      else
        printf "\"ENABLED\"%b to enable this feature" "$(bash_styling "reset_all")"
      fi
    )"

  printf "%bRemember to run %bglupis build%b to implement the latest edits of your CLI %b^_^%b\n" \
    "$(bash_styling "bold_light_purple")" \
    "$(bash_styling "bold_light_blue")" \
    "$(bash_styling "bold_light_purple")" \
    "$(bash_styling "bold_pink")" \
    "$(bash_styling "reset_all")"
}

initiate_new_session

# Setting Environment Variables for Directories and Files
export HOME="/c/Users/$USERNAME"
KELSIA_HOME="/c/Users/kelsia"
GIT_BASH_ALIASES_FILE="$KELSIA_HOME/AppData/Local/Programs/Git/etc/profile.d/aliases.sh"
KELSIA_BASH_PROFILE_FILE="$KELSIA_HOME/.bash_profile"
GLUPIS_BUILD_FILE="$HOME"/WebStormProjects/glupis/glupis/lib/build.sh

# Builds glupis Latest CLI Edits on New Session Start!
if [ "$ENABLE_BUILD" == "ENABLED" ]; then
  source "$GLUPIS_BUILD_FILE" && build
else
  source "$GLUPIS_BUILD_FILE" && make-executable
fi

# Updates aliases.sh and .bash_profile Without Opening a New Session
updatealiases() {
  source "$GIT_BASH_ALIASES_FILE"
  (cat "$GIT_BASH_ALIASES_FILE" > "$KELSIA_BASH_PROFILE_FILE") > /dev/null
  source "$KELSIA_BASH_PROFILE_FILE" > /dev/null
}

add_alias() {
  # Incorrect Syntax Provided
  if [[ "$(is_empty "$1")" == 1 ]] || [[ "$(is_empty "$2")" == 1 ]]; then
    printf "%bError adding new alias: Incorrect syntax%b\n" \
      "$(bash_styling "bold_red")" \
      "$(bash_styling "reset_all")"
    printf "  Usage:%b addalias ALIAS_NAME \"COMMAND\"%b" \
      "$(bash_styling "bold_light_blue")" \
      "$(bash_styling "reset_all")"

  # Alias Already Exists
  elif grep -q "alias ${1}=" "$GIT_BASH_ALIASES_FILE"; then
    printf "%bError adding new alias: Alias already exists\n" "$(bash_styling "bold_red")"
    printf "    %b>> %s\n\n" "$(bash_styling "bold_yellow")" "$(grep "alias ${1}=" "$GIT_BASH_ALIASES_FILE")"
    printf "%bUse%b updatealias OLD_ALIAS_NAME NEW_ALIAS_NAME \"NEW_COMMAND\"%b instead\n" \
      "$(bash_styling "reset_all")" \
      "$(bash_styling "bold_light_blue")" \
      "$(bash_styling "reset_all")"

  # Successfully Adding New Alias
  else
    (echo -e "\nalias ${1}='${2//\'/\"}'" >> "$GIT_BASH_ALIASES_FILE") && source "$GIT_BASH_ALIASES_FILE"
    source "$KELSIA_BASH_PROFILE_FILE" > /dev/null
  fi
}

test_args() {
  OPTIND=1
  ALIAS=""
  NEW_NAME=""

  # Transform Long Arguments to Short Arguments
  for ARG in "$@"; do
    shift
    case "$ARG" in
      '--rename')
        echo "found --rename"
        set -- "$@" '-r'
        ;;
      *)
        echo "not found: $ARG"
        set -- "$@" "$ARG"
        ;;
    esac
  done

  # If the Unnamed Arguments Are First
  if [[ ! "$1" =~ "-" ]]; then
    ALIAS="$1"
    shift
  fi

  # Short Arguments
  while getopts r: ARG; do
    echo "ARG in getopts: $ARG"
    case $ARG in
      r) NEW_NAME=$OPTARG ;;
      *) usage ;;
    esac
    shift
    shift
  done

  # If the Unnamed Arguments Were Last
  if [ -z "$ALIAS" ]; then
    ALIAS="$1"
  fi

  echo "ALIAS: $ALIAS"
  echo "NEW_NAME: $NEW_NAME"
}

# TODO: Implement! But I May Have Accidentally Already Hmm...
# rename_alias() {
#
# }

# TODO: I Don't Think I Ever Got Around to Fixing This...?
update_alias() {
  OPTIND=1
  ALIAS="$1"
  NEW_COMMANDS=("$@")
  OLD_ALIAS_NAME=""
  NEW_ALIAS_NAME=""
  COMMAND=""

  # TRANSFORM LONG ARGUMENTS TO SHORT ARGUMENTS
  for ARG in "$@"; do
    shift
    case "$ARG" in
      '--rename') set -- "$@" '-r' ;;
      *) set -- "$@" "$ARG" ;;
    esac
  done

  # SHORT ARGUMENTS
  while getopts r: ARG; do
    case $ARG in
      r) NEW_NAME=$OPTARG ;;
      *) usage ;;
    esac
  done

  parse_command() {
    if [[ "$(is_empty "$1")" == 1 ]]; then
      echo "COMMAND IS EMPTY"
      NEW_COMMAND="$(grep -n "alias $2=" "$GIT_BASH_ALIASES_FILE" | cut -d "=" -f2 | sed 's/^.//' | sed 's/.$//')"
    else
      echo "COMMAND IS NOT EMPTY"
      NEW_COMMAND="${1//\'/\"}"
    fi

    add_alias "$3" "$NEW_COMMAND" && delete_alias "$2"
  }

  if [[ ("$(is_empty "$OLD_ALIAS_NAME")" == 1 && "$(is_empty "$NEW_ALIAS_NAME")" == 1 && "$(is_empty "$ALIAS")" == 1) ]] || [[ (("$(is_empty "$OLD_ALIAS_NAME")" == 0 || "$(is_empty "$NEW_ALIAS_NAME")" == 0) && "$(is_empty "$ALIAS")" == 0) ]]; then
    echo -e "Error updating alias\n\n  Use the syntax:\n  updatealias -o \$OLD_ALIAS_NAME -n \$NEW_ALIAS_NAME -c \"\$COMMAND\"\nor\n  updatealias -a \$ALIAS -c \"\$COMMAND\""
  elif ! grep -q "alias $OLD_ALIAS_NAME=" "$GIT_BASH_ALIASES_FILE" && ! grep -q "alias $ALIAS=" "$GIT_BASH_ALIASES_FILE"; then
    echo -e "Error updating alias\n\n  Alias doesn't exist\n\n  Use addalias instead:\n  addalias \$ALIAS_NAME \"\$COMMAND\""
  else
    echo "FOUND ALIAS"
    if [[ "$(is_empty "$ALIAS")" == 1 ]]; then
      LINE_NUMBER="$(grep -n "alias $OLD_ALIAS_NAME=" "$GIT_BASH_ALIASES_FILE" | cut -d ":" -f1)"
      parse_command "$COMMAND" "$OLD_ALIAS_NAME" "$NEW_ALIAS_NAME" "$LINE_NUMBER"
    else
      LINE_NUMBER="$(grep -n "alias $ALIAS=" "$GIT_BASH_ALIASES_FILE" | cut -d ":" -f1)"
      parse_command "$COMMAND" "$ALIAS" "$ALIAS" "$LINE_NUMBER"
    fi
  fi
  source "$GIT_BASH_ALIASES_FILE" && source "$KELSIA_BASH_PROFILE_FILE" # && source "$WINDOWS_BASHRC_FILE"
}

delete_alias() {
  if [[ "$(is_empty "$1")" == 1 ]]; then
    echo -e "Error deleting alias\n\n  Use the syntax:\n  deletealias \$ALIAS_NAME"
  elif ! grep -q "alias ${1}=" "$GIT_BASH_ALIASES_FILE"; then
    echo -e "Error deleting alias\n\n  Alias doesn't exist"
  else
    unalias "${1}"
    LINE_NUMBER="$(grep -n "alias ${1}=" "$GIT_BASH_ALIASES_FILE" | cut -d ":" -f1)"
    sed -i "${LINE_NUMBER}d" "$GIT_BASH_ALIASES_FILE"
  fi
  source "$GIT_BASH_ALIASES_FILE" && source "$KELSIA_BASH_PROFILE_FILE" # && source "$WINDOWS_BASHRC_FILE"
}

is_empty() {
  if [[ -n $1 ]] && [[ $1 != " " ]]; then
    echo "0"
  else
    echo "1"
  fi
}

replace() {
  echo "${1//\'/\"}"
}

set_env_var() {
  export "$1"="$2" && setx "$1" "$2"
}

set_glab_config() {
  glab config set host "$1" && glab config set token "$2"
}

update_git_ignore() {
  # TODO: Add check for if git status returns ./idea
  echo -e "${BLUE}UPDATING \".gitignore\" FILE TO EXCLUDE THE \".idea\" DIRECTORY$(bash_styling "reset_all")"
  echo "/**" > ".idea/.gitignore"
}

git_update() {
  if [[ -z $1 ]]; then
    echo -e "${RED}Please provide a commit message$(bash_styling "reset_all")"
    echo -e "Ex:\n  update \"A really descriptive commit message\""
  else
    update_git_ignore &&
      (
        echo -e "${BLUE}RUNNING \"git status\"$(bash_styling "reset_all")"
        git status
      ) &&
      (
        echo -e "${BLUE}RUNNING \"git add .\"$(bash_styling "reset_all")"
        git add .
      ) &&
      (
        echo -e "${BLUE}RUNNING \"git commit -m $1\"$(bash_styling "reset_all")"
        git commit -m "$1"
      ) &&
      (
        echo -e "${BLUE}RUNNING \"git push\"$(bash_styling "reset_all")"
        git push ||
          (
            echo -e "${RED}NO UPSTREAM BRANCH SET, ASSUMING \"origin/main\" AS THE UPSTREAM BRANCH$(bash_styling "reset_all")"
            echo -e "${BLUE}RUNNING \"git push --set-upstream origin main\"$(bash_styling "reset_all")"
            git push --set-upstream origin main
          )
      )
  fi
}

# I gueessss I won't Enforce My CLI Settings on Other Users! :(
if [ -z "$USER" ]; then
  if [ "$USERNAME" != "kelsia" ]; then
    if [ ! -d "$HOME"/oh-my-posh-themes ]; then
      ln -s "$KELSIA_HOME/oh-my-posh-themes" oh-my-posh-themes
    fi
  fi
fi

# Oh My Posh Settings!
eval "$(oh-my-posh init bash --config "$HOME"/oh-my-posh-themes/config.json)"

# We Love a Pretty `ls` Command!
alias ls='ls -la --color=tty'

# Aliases
alias addalias='add_alias'
alias updatealias='update_alias'
alias deletealias='delete_alias'
alias setenv='set_env_var'
alias setglabconfig='set_glab_config'
alias update='git_update'

# Update Alias File for Git Bash
alias updatealiases='updatealiases'
alias editaliases='aliasdir && webstorm aliases.sh "$HOME"/.bashrc'

# Execute cmd Files Properly in Git Bash
# TODO: Experiments Failed :( Revisit!

# Execute Interactive Commands Properly
# TODO: Experiments Failed :( Revisit!

# EXE Shortcuts
alias webstorm='webstorm64'
alias ws='webstorm'

# Directory Shortcuts
alias home='cd $KELSIA_HOME'
alias aliasdir='home && cd AppData/Local/Programs/Git/etc/profile.d'

# TODO: Automate this for directory creation? :)

# Code Directory
alias code='home && cd WebstormProjects'

# Short Project Directory Shortcuts

# Not Yet Sorted Aliases

alias cursor='echo -e [?25h'
alias hidecursor='echo -e [?25l'
alias shebang='echo "#!/bin/bash"'
alias scripts='code && cd Scripts'
alias cli='code && cd glupis'

alias glupisdir='cli && cd glupis'
alias editawsconfig='ws $HOME/.aws/config $HOME/.aws/credentials $HOME/.kochid/aws.yaml'
alias showr='sed "s/\n/§/g"'

# Experimental wt Settings
# alias ubuntu='wt -p "Ubuntu"'
# alias pink='wt nt --tabColor "#aa00bd"'
# alias blue='wt nt --tabColor "#4287f5"'
# alias teal='wt nt --tabColor "#00ab9a"'
# alias purple='wt nt --tabColor "#5000ab"'
# alias red='wt nt --tabColor "#ff0022"'
# alias orange='wt nt --tabColor "#ff5500"'
# alias green='wt nt --tabColor "#10731a"'

# Windows and AWS SAM :(
alias sam='C:/PROGRA~1/Amazon/AWSSAMCLI/bin/sam.cmd'